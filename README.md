# Blog8965 Corpus

## Overview
The Blog8965 corpus is an English authorship attribution testbed for contemporary English prose.
It has 8,965 candidate authors, 542k+ posts, and pre-defined data split (train/dev/test proportional to ca. 8:1:1).
It is a subset of the [Blog Authorship Corpus (BAC)](https://www.kaggle.com/datasets/rtatman/blog-authorship-corpus). 
[Download the corpus](https://drive.google.com/file/d/1_OmktFoVcjYk7AFt4ulLzYSu44mMBGn8/view?usp=sharing). 

# Preprocessing

1. We filter out very short posts (i.e., posts shorter than 100 characters).
2. We remove all posts are not likely written in English with the help from `langdetect` package (v1.0.9). Any post that leads to `langdetect.detect()` failling to predict as 'en' is removed. Common cases include posts with only spaces, urls, emoticons, and posts written in other languages (e.g., in Dutch).
2. We select authors whom have at least 10 posts and collectively no less than 8,000 characters. Authors are identfied using the BAC's `id` field.

Coding and some stats can be found in the jupyter notebook (blog8965_generation_and_stats.ipynb).

|    Split   | No. Authors  |           No. Posts         | No. Character per Author (s.d.)  | No. Posts per Author (s.d.) | 
|------------|:------------:|:---------------------------:|:--------------------------------:|:---------------------------:|
| Train      |     8,965    |           433,838           |          60,582(110,841)         |           48(99)            |
| Validation |     8,373    |            54,230           |          8,005(14,928)           |            6(13)            |
| Test       |     8,347    |            54,230           |          8,077(14,807)           |            6(13)            |


## Usage

```{python}
import pandas as pd

df = pd.read_csv('blog8965.csv.gz')

# read in training data
train_text, train_label = zip(*df.loc[df.split=='train'][['text', 'id']].itertuples(index=False))
```

## License
All the materials is licensed under the ISC License.


## Contact
Contact [the repo maintainer](mailto:hw56@indiana.edu) for questions and bugs.
